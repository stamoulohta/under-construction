<?php
/**
 * @package underconstruction
 */

/*
Plugin Name: Under Construction
Plugin URI: http://stamoulohta.com/#_under_construction
Description: Redirects to an "under construction" page if user is'nt logged in.
Version: 0.a.1
Author: Stamoulohta
Author URI: http://stamoulohta.com
License: GPLv2 or later
Text Domain: underconstruction
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Copyright 2018 Stamoulohta, Inc.
*/

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo('Called out of context error.');
	exit;
}

define('UC_URL', 'uc/');

add_action('send_headers', 'uc_redirect');

function uc_redirect(){
    if(!is_user_logged_in()){
        wp_redirect(UC_URL);
        exit;
    }
}
